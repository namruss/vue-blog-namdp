export const LISTCATEGORY=[
    {key:0, value:'Kinh Doanh'},
    {key:1, value:'Thế Giới'},
    {key:2, value:'Giải Trí'},
    {key:3, value:'Thời Sự'},
    {key:4, value:'Thế Giới'},
    {key:5, value:'Khoa Học'},
    {key:6, value:'Công Nghệ Và Đời Sống'},
    {key:7, value:'Tâm Lý Con Người'},
    {key:8, value:'Viễn Tưởng'},
    {key:9, value:'Động Vật'},
    {key:10, value:'Sinh Vật'},
];

export const LISTPOSITION=[
    {key:0, value:'Việt Nam'},
    {key:1, value:'Châu Âu'},
    {key:2, value:'Châu Mỹ'},
    {key:3, value:'Châu Á'},
    {key:4, value:'Châu Phi'},
    {key:5, value:'Châu Đại Dương'},
    {key:6, value:'Đông Nam Á'},
]